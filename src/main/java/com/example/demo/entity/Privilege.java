package com.example.demo.entity;

public class Privilege {
    private Integer ID;
    private String privilegeName;
    private String privilegeNumber;
    private String privilegeTipflag;
    private String privilegeTypeflag;
    private String privilegeUrl;
    private String icon;

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName;
    }

    public String getPrivilegeNumber() {
        return privilegeNumber;
    }

    public void setPrivilegeNumber(String privilegeNumber) {
        this.privilegeNumber = privilegeNumber;
    }

    public String getPrivilegeTipflag() {
        return privilegeTipflag;
    }

    public void setPrivilegeTipflag(String privilegeTipflag) {
        this.privilegeTipflag = privilegeTipflag;
    }

    public String getPrivilegeTypeflag() {
        return privilegeTypeflag;
    }

    public void setPrivilegeTypeflag(String privilegeTypeflag) {
        this.privilegeTypeflag = privilegeTypeflag;
    }

    public String getPrivilegeUrl() {
        return privilegeUrl;
    }

    public void setPrivilegeUrl(String privilegeUrl) {
        this.privilegeUrl = privilegeUrl;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "Privilege{" +
                "ID=" + ID +
                ", privilegeName='" + privilegeName + '\'' +
                ", privilegeNumber='" + privilegeNumber + '\'' +
                ", privilegeTipflag='" + privilegeTipflag + '\'' +
                ", privilegeTypeflag='" + privilegeTypeflag + '\'' +
                ", privilegeUrl='" + privilegeUrl + '\'' +
                ", icon='" + icon + '\'' +
                '}';
    }
}
