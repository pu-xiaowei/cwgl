package com.example.demo.entity;


import java.math.BigDecimal;

public class Debt {
    private Integer id;
    private Integer userid;
    private String houseid;
    private String name;
    private BigDecimal residue;
    private BigDecimal curperiod;
    private String realname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getHouseid() {
        return houseid;
    }

    public void setHouseid(String houseid) {
        this.houseid = houseid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getResidue() {
        return residue;
    }

    public void setResidue(BigDecimal residue) {
        this.residue = residue;
    }

    public BigDecimal getCurperiod() {
        return curperiod;
    }

    public void setCurperiod(BigDecimal curperiod) {
        this.curperiod = curperiod;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public Debt(Integer id, Integer userid, String houseid, String name, BigDecimal residue, BigDecimal curperiod) {
        this.id = id;
        this.userid = userid;
        this.houseid = houseid;
        this.name = name;
        this.residue = residue;
        this.curperiod = curperiod;
    }

    public Debt() {
    }
}
