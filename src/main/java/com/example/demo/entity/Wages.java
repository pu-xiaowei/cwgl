package com.example.demo.entity;

import java.math.BigDecimal;

public class Wages {
    private Integer id;
    private BigDecimal basewage;
    private BigDecimal gradewage;
    private BigDecimal performance;
    private BigDecimal otherwage;
    private BigDecimal fiveinsurance;
    private BigDecimal tax;
    private BigDecimal otherpayment;
    private BigDecimal sumwage;
    private Integer userid;
    private String houseid;
    private String  createtime;
    private String realname;

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getHouseid() {
        return houseid;
    }

    public void setHouseid(String houseid) {
        this.houseid = houseid;
    }

    public Wages() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getBasewage() {
        return basewage;
    }

    public void setBasewage(BigDecimal basewage) {
        this.basewage = basewage;
    }

    public BigDecimal getGradewage() {
        return gradewage;
    }

    public void setGradewage(BigDecimal gradewage) {
        this.gradewage = gradewage;
    }

    public BigDecimal getPerformance() {
        return performance;
    }

    public void setPerformance(BigDecimal performance) {
        this.performance = performance;
    }

    public BigDecimal getOtherwage() {
        return otherwage;
    }

    public void setOtherwage(BigDecimal otherwage) {
        this.otherwage = otherwage;
    }

    public BigDecimal getFiveinsurance() {
        return fiveinsurance;
    }

    public void setFiveinsurance(BigDecimal fiveinsurance) {
        this.fiveinsurance = fiveinsurance;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public BigDecimal getOtherpayment() {
        return otherpayment;
    }

    public void setOtherpayment(BigDecimal otherpayment) {
        this.otherpayment = otherpayment;
    }

    public BigDecimal getSumwage() {
        return sumwage;
    }

    public void setSumwage(BigDecimal sumwage) {
        this.sumwage = sumwage;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }
}
