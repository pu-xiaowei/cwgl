package com.example.demo.dao;


import com.example.demo.entity.Bill;
import com.example.demo.entity.Debt;
import com.example.demo.utils.PageModel;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface DebtMapper{
    int add(Debt debt);

    int update(Debt debt);

    int del(int id);

    List<Debt> findByWhere(PageModel<Debt> model);

    List<Debt> findByWhereNoPage(Debt model);

    int getTotalByWhere(PageModel<Bill> model);

    Debt select(int id);
}
