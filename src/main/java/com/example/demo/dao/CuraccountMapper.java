package com.example.demo.dao;

import com.example.demo.entity.Bill;
import com.example.demo.entity.Curaccount;
import com.example.demo.utils.PageModel;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CuraccountMapper{
    int add(Curaccount curaccount);

    int update(Curaccount curaccount);

    int del(int id);

    List<Curaccount> findByWhere(PageModel<Curaccount> model);

    List<Curaccount> findByWhereNoPage(Curaccount model);

    int getTotalByWhere(PageModel<Bill> model);

    Curaccount queryOneByBill(Bill bill);

    String getMoney(Integer userid);
}
