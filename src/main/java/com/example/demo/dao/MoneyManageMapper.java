package com.example.demo.dao;



import com.example.demo.entity.Bill;
import com.example.demo.entity.MoneyManage;
import com.example.demo.utils.PageModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MoneyManageMapper {

    int add(MoneyManage moneyManage);

    int update(MoneyManage moneyManage);

    int del(int id);

    List<MoneyManage> findByWhere(PageModel<MoneyManage> model);

   List<MoneyManage> findByWhereNoPage(MoneyManage model);

    int getTotalByWhere(PageModel<Bill> model);


}
