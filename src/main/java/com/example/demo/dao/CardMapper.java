package com.example.demo.dao;

import com.example.demo.entity.Card;
import com.example.demo.utils.PageModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardMapper {

    //List<Product> getProducts(PageModel model);

    //Integer getProductsTotal(PageModel model);

    int del(int id);

    int add(Card card);

    //List<Product> findByWhereNoPage(Product product);

    int update(Card card);

    List<Card> findByWhere(PageModel<Card> model);

    int getTotalByWhere(PageModel model);
}
