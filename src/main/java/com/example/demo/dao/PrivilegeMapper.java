package com.example.demo.dao;

import com.example.demo.entity.Privilege;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrivilegeMapper {
    List<Privilege> getPrivilegeByRoleid(int roleid);
    int addDefaultPrivilegesWhenAddRole(String roleid);
    int delPrivilegesWenDelRole(String roleid);

}