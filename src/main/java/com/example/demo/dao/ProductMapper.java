package com.example.demo.dao;

import com.example.demo.entity.Product;
import com.example.demo.utils.PageModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductMapper {
    List<Product> getProducts(PageModel model);

    Integer getProductsTotal(PageModel model);

    int del(int id);

    int add(Product product);

    List<Product> findByWhereNoPage(Product product);

    int update(Product product);
}
