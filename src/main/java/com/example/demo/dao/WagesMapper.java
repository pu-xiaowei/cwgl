package com.example.demo.dao;

import com.example.demo.entity.Wages;
import com.example.demo.utils.PageModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WagesMapper {

    int del(int id);

    int add(Wages wages);

    //List<Product> findByWhereNoPage(Product product);

    int update(Wages wages);

    List<Wages> findByWhere(PageModel<Wages> model);

    int getTotalByWhere(PageModel model);
}
