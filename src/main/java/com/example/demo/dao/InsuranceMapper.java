package com.example.demo.dao;

import com.example.demo.entity.Insurance;
import com.example.demo.utils.PageModel;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface InsuranceMapper {

    int del(int id);

    int add(Insurance insurance);


    int update(Insurance insurance);

    List<Insurance> findByWhere(PageModel<Insurance> model);

    int getTotalByWhere(PageModel model);
}
