package com.example.demo.controller;


import com.example.demo.entity.Curaccount;
import com.example.demo.entity.UserInfo;
import com.example.demo.service.CuraccountService;
import com.example.demo.utils.Config;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;
import com.example.demo.utils.ResultUtil;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

@RestController
@RequestMapping("/curaccount")
public class CuraccountController {
    @Resource
    private CuraccountService curaccountService;



    @RequestMapping("/getCuraccount/{pageNo}/{pageSize}")
    public Result<Curaccount> getBillsByWhere(Curaccount curaccount, @PathVariable int pageNo, @PathVariable int pageSize, HttpSession session){
        curaccount = getHouseCuraccount(curaccount,session);
        PageModel model = new PageModel<>(pageNo,curaccount);
        model.setPageSize(pageSize);
        return curaccountService.findByWhere(model);
    }


    private Curaccount getHouseCuraccount(Curaccount curaccount, HttpSession session) {
        UserInfo currentUser = Config.getSessionUser(session);
        //当登录用户为家主时，查询默认查询全家账单情况
        //当登录用户为普通用户时，仅查询当前用户的账单
        if (currentUser.getRoleid() == 2){
            curaccount.setHouseid(currentUser.getHouseid());
        }else if (currentUser.getRoleid() == 3){
            curaccount.setUserid(currentUser.getId());
        }
        return curaccount;
    }

    @RequestMapping("/getMoney")
    public Result getMoney(HttpSession session){
        Integer userid = null;
        if (Config.getSessionUser(session)!=null){
            userid = Config.getSessionUser(session).getId();
        }
        String moneyStr = curaccountService.getMoney(userid);
        Double money = Double.valueOf(moneyStr==null ? "0" : moneyStr);
        double v = 0.3;
        Double s = money * v;
        HashMap<String, String> map = new HashMap<>();
        map.put("money", money.toString());
        map.put("money3", String.format("%.2f", s));
        return ResultUtil.success(map);
    }


}

