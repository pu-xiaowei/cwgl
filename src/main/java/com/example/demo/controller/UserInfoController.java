package com.example.demo.controller;

import com.example.demo.configs.Md5UtilSimple;
import com.example.demo.dao.UserInfoMapper;
import com.example.demo.entity.Privilege;
import com.example.demo.entity.Role;
import com.example.demo.entity.RoleVo;
import com.example.demo.entity.UserInfo;
import com.example.demo.service.PrivilegeService;
import com.example.demo.service.UserInfoService;
import com.example.demo.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class UserInfoController {
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Resource
    private UserInfoService userInfoService;
    @Resource
    private PrivilegeService privilegeService;

    /**
     * 进入登录界面
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = {"/", "login.html"})
    public String toLogin(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        if (session.getAttribute(Config.CURRENT_USERNAME) == null) {
            return "login";
        } else {
            try {
//                return "redirect:index";
                response.sendRedirect("/pages/index");
            } catch (IOException e) {
                e.printStackTrace();
                return "login";
            }
            return null;
        }
    }

    /**
     * 进入注册页面
     *
     * @return
     */
    @RequestMapping(value = {"/register.html"})
    public String toRegister() {
        return "register";
    }

    /**
     * 退出登录,移除session和cookie
     *
     * @param request
     * @param response
     * @return 进入login, html
     */
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        delCookieUser(request, response);
        request.getSession().removeAttribute(Config.CURRENT_USERNAME);
        return "login";
    }

    /**
     * 表单、登录验证
     *
     * @param userInfo
     * @param request
     * @param response
     * @return 返回结果
     */
    @RequestMapping("/login.do")
    @ResponseBody
    public Result getUserInfo(UserInfo userInfo, HttpServletRequest request,
                              HttpServletResponse response) {
        System.out.println("登录信息：" + userInfo);
        if (!CodeUtil.checkVerifyCode(request)) {
            return ResultUtil.unSuccess("验证码输入错误");
        }
        //将密码加密处理
//        userInfo.setPassword(Md5UtilSimple.md5(userInfo.getPassword()));
        System.out.println(userInfo);
        boolean userIsExisted = userInfoService.userIsExisted(userInfo);
        userInfo = userInfoService.getUserInfo(userInfo);
        if ("client".equals(request.getHeader("token")) && !userIsExisted) {
            return ResultUtil.success(-1);
        }
        if (!userIsExisted || userInfo == null) {
            System.out.println("用户名或密码错误!");
            return ResultUtil.unSuccess("用户名或密码错误!");
        } else {
            System.out.println("查询到用户为" + userInfo.getUsername());
            userInfo = setSessionUserInfo(userInfo, request.getSession());
            setCookieUser(request, response);
            System.out.println("登录成功!");
            return ResultUtil.success("登录成功", userInfo);
        }
    }

    /**
     * 注册验证
     *
     * @param userInfo
     * @return
     */
    @RequestMapping("/register.do")
    @ResponseBody
    public Result addUserInfo(UserInfo userInfo) {
        System.out.println("注册信息：" + userInfo);
        boolean userIsExisted = userInfoService.userIsExisted(userInfo);
        if (userIsExisted) {
            return ResultUtil.unSuccess("用户已经存在");
        }
        if (userInfoService.addUser(userInfo) > 0) {
            return ResultUtil.success("注册成功");
        } else {
            return ResultUtil.unSuccess("注册失败");
        }
    }

    /**
     * 用户修改密码
     *
     * @param userInfo
     * @param newPassword
     * @param reNewPassword
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/user/password/change")
    public @ResponseBody
    Result updateUserPassword(UserInfo userInfo, @RequestParam(name = "newPassword") String newPassword, @RequestParam(name = "reNewPassword") String reNewPassword, HttpServletRequest request, HttpServletResponse response) {
        if (!reNewPassword.equals(newPassword)) {
            return ResultUtil.unSuccess("两次新密码不一致！");
        }
        String old_password = userInfo.getPassword();
        UserInfo user = getUserInfo(userInfo);
        if (!userInfoService.userIsExisted(userInfo)) {
            return ResultUtil.unSuccess("用户不存在！");
        }
        if (!Md5UtilSimple.md5(old_password).equals(user.getPassword())) {
            return ResultUtil.unSuccess("原密码错误！");
        }
        try {
            user.setPassword(Md5UtilSimple.md5(newPassword));
            int num = userInfoService.changePassword(userInfo, user.getPassword());
            if (num > 0) {
                delCookieUser(request, response);
                request.getSession().removeAttribute(Config.CURRENT_USERNAME);
                System.out.println(request.getSession().getAttribute(Config.CURRENT_USERNAME));
                return ResultUtil.success();
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    /**
     * 更新用户数据
     *
     * @param userInfo
     * @return
     */
    @RequestMapping("/user/update")
    public @ResponseBody
    Result updateUser(UserInfo userInfo) {
        try {
            int num = userInfoService.update(userInfo);
            if (num > 0) {
                return ResultUtil.success();
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    /**
     * 删除用户
     *
     * @param id
     * @return
     */
    @RequestMapping("/user/del/{id}")
    public @ResponseBody
    Result deleteUser(@PathVariable String id) {
        System.out.println("获取道德id+" + id);
        try {
            int num = userInfoService.delete(id);
            if (num > 0) {
                return ResultUtil.success();
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @RequestMapping("/users/getUsersByWhere/{pageNo}/{pageSize}")
    public @ResponseBody
    Result getUsersByWhere(UserInfo userInfo, @PathVariable int pageNo,
                           @PathVariable int pageSize, int role, HttpSession session) {
        if (1 == role) {
            userInfo.setHouseid("");
        }
        Utils.log(userInfo.toString());
        PageModel model = new PageModel<>(pageNo, userInfo);
        model.setPageSize(pageSize);
        return userInfoService.getUsersByWhere(model);
    }

    /**
     * 添加用户
     * @param userInfo
     * @return
     */
    @RequestMapping("/user/add")
    public @ResponseBody
    Result addUser(UserInfo userInfo,HttpSession session) {
        boolean userIsExisted = userInfoService.userIsExisted(userInfo);
        if (userIsExisted) {
            return ResultUtil.unSuccess("用户已存在！");
        }
        if (Config.getSessionUser(session)!=null){
            userInfo.setHouseid(Config.getSessionUser(session).getHouseid());
        }
        System.out.println("添加用户信息"+userInfo);
        try {
//            userInfo.setPassword(Md5UtilSimple.md5(userInfo.getPassword()));
            int num = userInfoService.add(userInfo);
            if (num > 0) {
                return ResultUtil.success();
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }


    /**
     * 获取所有角色信息
     *
     * @return
     */
    @RequestMapping("/getAllRoles")
    public @ResponseBody
    Result<Role> getAllRoles() {
        try {
            List<Role> roles = userInfoService.getAllRoles();
            if (roles.size() > 0) {
                return ResultUtil.success(roles);
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @RequestMapping("/getRoles")
    public @ResponseBody
    Result<Role> getRoles() {
        try {
            List<Role> roles = userInfoService.getRoles();
            if (roles.size() > 0) {
                return ResultUtil.success(roles);
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    /**
     * 添加角色
     *
     * @param role
     * @return
     */
    @RequestMapping("/role/add")
    public @ResponseBody
    Result addRole(RoleVo role) {
        Role role1 = new Role();
        role1.setRolename(role.getName());
        Role role2 = userInfoMapper.selectRoleByName1(role.getName());
        if (role2 != null) {
            return ResultUtil.unSuccess("该角色已存在");
        }
        userInfoService.addRole(role1);
        String[] id = role.getId();
        if (id != null) {
            id = deleteArrayNull(id);
        }
        if (id != null && id.length > 0) {
            String join = String.join(",", id);
            if (join.indexOf("65") > 0 || join.indexOf("63") > 0) {
                join += ",62";
            }
            if (join.indexOf("75") > 0 || join.indexOf("76") > 0 || join.indexOf("77") > 0 || join.indexOf("78") > 0 || join.indexOf("79") > 0 || join.indexOf("80") > 0 || join.indexOf("81") > 0) {
                join += ",64";
            }
            if (join.indexOf("67") > 0) {
                join += ",66";
            }
            if (join.indexOf("74") > 0) {
                join += ",68";
            }
            if (join.indexOf("70") > 0 || join.indexOf("71") > 0) {
                join += ",69";
            }
            String[] split = join.split(",");
            for (int i1 = 0; i1 < split.length; i1++) {
                userInfoMapper.insertPrivileges(role1.getRoleid(), split[i1]);
            }
        }
        return ResultUtil.success();
    }

    /**
     * 更新角色信息
     *
     * @param role
     * @return
     */
    @RequestMapping("/role/update")
    public @ResponseBody
    Result updateRole(RoleVo role) {
        userInfoMapper.deletePrivilege(role.getRoleid());
        //userInfoMapper.deleteRole(role.getRoleid());
        Role role2 = userInfoMapper.selectRoleByName(role.getName(), role.getRoleid());
        if (role2 != null) {
            return ResultUtil.unSuccess("该角色已存在");
        }
        Role role1 = new Role();
        role1.setRoleid(Integer.valueOf(role.getRoleid()));
        role1.setRolename(role.getName());
        userInfoMapper.updateRole(role1);
        String[] id = role.getId();
        if (id != null) {
            id = deleteArrayNull(id);
        }
        if (id != null && id.length > 0) {
            String join = String.join(",", id);
            if (join.indexOf("65") > 0 || join.indexOf("63") > 0) {
                join += ",62";
            }
            if (join.indexOf("75") > 0 || join.indexOf("76") > 0 || join.indexOf("77") > 0 || join.indexOf("78") > 0 || join.indexOf("79") > 0 || join.indexOf("80") > 0 || join.indexOf("81") > 0) {
                join += ",64";
            }
            if (join.indexOf("67") > 0) {
                join += ",66";
            }
            if (join.indexOf("74") > 0) {
                join += ",68";
            }
            if (join.indexOf("70") > 0 || join.indexOf("71") > 0) {
                join += ",69";
            }
            String[] split = join.split(",");
            for (int i1 = 0; i1 < split.length; i1++) {
                userInfoMapper.insertPrivileges(role1.getRoleid(), split[i1]);
            }
        }
        return ResultUtil.success();
    }


    /**
     * 删除角色信息
     *
     * @param roleid
     * @return
     */
    @RequestMapping("/role/del/{roleid}")
    public @ResponseBody
    Result deleteRole(@PathVariable String roleid) {
        userInfoMapper.deletePrivilege(roleid);
        userInfoMapper.deleteRole(roleid);
        return ResultUtil.success();
    }

    @RequestMapping("/getRole/{id}")
    public @ResponseBody
    Result getRoleById(@PathVariable String id) {
        try {
            Role role = userInfoService.getRoleById(id);
            if (role != null) {
                return ResultUtil.success(role);
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @RequestMapping("/getAllPrivilege")
    public @ResponseBody
    String getAllPrivilege(String roleId) {
        List<String> ids = userInfoService.getAllPrivilege(roleId);
        if (!CollectionUtils.isEmpty(ids)) {
            String join = String.join(",", ids);
            if (join.indexOf("65") > 0 || join.indexOf("63") > 0) {
                join += ",62";
            }
            if (join.indexOf("75") > 0 || join.indexOf("76") > 0 || join.indexOf("77") > 0 || join.indexOf("78") > 0 || join.indexOf("79") > 0 || join.indexOf("80") > 0 || join.indexOf("81") > 0) {
                join += ",64";
            }
            if (join.indexOf("67") > 0) {
                join += ",66";
            }
            if (join.indexOf("74") > 0) {
                join += ",68";
            }
            if (join.indexOf("70") > 0 || join.indexOf("71") > 0) {
                join += ",69";
            }
            return join;
        }
        return "";
    }


    //注销时删除cookie信息
    private void delCookieUser(HttpServletRequest request, HttpServletResponse response) {
        UserInfo user = getSessionUser(request.getSession());
        Cookie cookie = new Cookie(Config.CURRENT_USERNAME, user.getUsername() + "_" + user.getId());
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }

    @RequestMapping("/getSessionUser")
    @ResponseBody
    public UserInfo getSessionUser(HttpSession session) {
        UserInfo sessionUser = (UserInfo) session.getAttribute(Config.CURRENT_USERNAME);
        sessionUser.setPassword(null);
        return sessionUser;
    }

    public UserInfo getUserInfo(UserInfo userInfo) {
        return userInfoService.getUserInfo(userInfo);
    }

    private void setCookieUser(HttpServletRequest request, HttpServletResponse response) {
        UserInfo user = getSessionUser(request.getSession());
        Cookie cookie = new Cookie(Config.CURRENT_USERNAME,
                user.getUsername() + "_" + user.getId());
        cookie.setMaxAge(60 * 60);
        response.addCookie(cookie);
    }

    public UserInfo setSessionUserInfo(UserInfo userInfo, HttpSession session) {
        //获取管理特权
        List<Privilege> privileges = privilegeService.getPrivilegeByRoleid(userInfo.getRoleid());
        userInfo.setPrivileges(privileges);
        System.out.println("用户获取特权之后信息" + userInfo);
        session.setAttribute(Config.CURRENT_USERNAME, userInfo);
        return userInfo;
    }

    /***
     * 去除String数组中的空值
     */
    private String[] deleteArrayNull(String string[]) {
        String strArr[] = string;

        // step1: 定义一个list列表，并循环赋值
        ArrayList<String> strList = new ArrayList<String>();
        for (int i = 0; i < strArr.length; i++) {
            strList.add(strArr[i]);
        }

        // step2: 删除list列表中所有的空值
        while (strList.remove(null)) ;
        while (strList.remove("")) ;

        // step3: 把list列表转换给一个新定义的中间数组，并赋值给它
        String strArrLast[] = strList.toArray(new String[strList.size()]);

        return strArrLast;
    }
}
