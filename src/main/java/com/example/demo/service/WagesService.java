package com.example.demo.service;

import com.example.demo.entity.Wages;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;

public interface WagesService {

    Result<Wages> getWages(PageModel model);

    int add(Wages wages);

    int update(Wages wages);

    void del(int id);
}
