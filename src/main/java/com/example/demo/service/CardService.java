package com.example.demo.service;


import com.example.demo.entity.Card;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;

public interface CardService {
    Result<Card> getCards(PageModel model);

    int addCard(Card card);

    int updateCard(Card card);

    void delCard(int id);
}
