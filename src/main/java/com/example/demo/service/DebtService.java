package com.example.demo.service;


import com.example.demo.entity.Debt;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;

public interface DebtService{

    Result<Debt> findByWhere(PageModel model);

    int add(Debt debt);

    int update(Debt debt);

    int del(int id);

    Debt select(int id);

    Result<Debt> findByWhereNoPage(Debt debt);
}
