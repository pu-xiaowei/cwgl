package com.example.demo.service;


import com.example.demo.entity.Bill;
import com.example.demo.entity.Payway;
import com.example.demo.entity.Product;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;

import java.util.List;
import java.util.Map;

public interface BillService {

    int add(Bill bill);

    int update(Bill bill);

    int del(int id);

    Result<Bill> findByWhere(PageModel model);

    Result<Bill> findByWhereNoPage(Bill bill);

    List<Payway> getAllPayways();

    List<Map<String,Float>>  getMonthlyInfo(PageModel<Bill> model);

    Result<Product> getProducts(PageModel model);

    int delProduct(int id);

    int addProduct(Product product);

    Result<Product>  findProductfPage(Product product);

    int updateProduct(Product product);
}
