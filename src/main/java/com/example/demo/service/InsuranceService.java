package com.example.demo.service;


import com.example.demo.entity.Insurance;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;

public interface InsuranceService {
    Result<Insurance> getInsurance(PageModel model);

    int addInsurance(Insurance insurance);


    int updateInsurance(Insurance insurance);

    void delInsurance(int id);
}
