package com.example.demo.service;

import com.example.demo.entity.Role;
import com.example.demo.entity.UserInfo;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;

import java.util.List;

public interface UserInfoService {
    int addUser(UserInfo userInfo);
    int add(UserInfo userInfo);
    int changePassword(UserInfo userInfo,String newPassword);
    int update(UserInfo userInfo);
    int delete(String id);
    boolean userIsExisted(UserInfo userInfo);
    UserInfo getUserInfo(UserInfo userInfo);
    Result getUsersByWhere(PageModel<UserInfo> model);
    List<Role> getAllRoles();
    List<Role> getRoles();
    Role getRoleById(String id);
    int addRole(Role role);
    int updateRole(Role role);
    int deleteRole(String id);
    List<String> getAllPrivilege(String roleId);
}
