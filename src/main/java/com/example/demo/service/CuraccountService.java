package com.example.demo.service;


import com.example.demo.entity.Curaccount;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;

public interface CuraccountService{
    Result<Curaccount> findByWhere(PageModel model);

    int add(Curaccount curaccount);

    int update(Curaccount curaccount);

    int del(int id);

    Result<Curaccount> findByWhereNoPage(Curaccount curaccount);

    String getMoney(Integer userid);
}
