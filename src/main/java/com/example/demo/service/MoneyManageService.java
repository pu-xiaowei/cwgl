package com.example.demo.service;

import com.example.demo.entity.MoneyManage;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;

public interface MoneyManageService {

    Result<MoneyManage> findByWhere(PageModel model);

    int add(MoneyManage moneyManage);

    int update(MoneyManage moneyManage);

    int del(int id);

    Result<MoneyManage> findByWhereNoPage(MoneyManage moneyManage);
}
