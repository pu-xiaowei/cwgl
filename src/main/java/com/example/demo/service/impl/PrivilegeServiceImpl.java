package com.example.demo.service.impl;

import com.example.demo.dao.PrivilegeMapper;
import com.example.demo.entity.Privilege;
import com.example.demo.service.PrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrivilegeServiceImpl implements PrivilegeService {
    @Autowired
    private PrivilegeMapper privilegeMapper;

    @Override
    public List<Privilege> getPrivilegeByRoleid(int roleid) {

        return this.privilegeMapper.getPrivilegeByRoleid(roleid);
    }


    @Override
    public int addDefaultPrivilegesWhenAddRole(String roleid) {
        return privilegeMapper.addDefaultPrivilegesWhenAddRole(roleid);
    }

    @Override
    public int delPrivilegesWenDelRole(String roleid) {
        return privilegeMapper.delPrivilegesWenDelRole(roleid);
    }
}
