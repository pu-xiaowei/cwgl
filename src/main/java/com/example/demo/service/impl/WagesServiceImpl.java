package com.example.demo.service.impl;

import com.example.demo.dao.WagesMapper;
import com.example.demo.entity.Wages;
import com.example.demo.service.WagesService;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;
import com.example.demo.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WagesServiceImpl implements WagesService {

    @Autowired
    WagesMapper mapper;


    @Override
    public Result<Wages> getWages(PageModel model) {
        try {
            List<Wages> wagesList = mapper.findByWhere(model);
            if (wagesList.size() >= 0) {
                Result<Wages> result = ResultUtil.success(wagesList);
                result.setTotal(mapper.getTotalByWhere(model));
                if (result.getTotal() == 0) {
                    result.setMsg("没有查到相关数据");
                } else {
                    result.setMsg("数据获取成功");
                }
                return result;
            } else {
                return ResultUtil.unSuccess("获取数据失败！");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @Override
    public int add(Wages wages) {
        return mapper.add(wages);
    }

    @Override
    public int update(Wages wages) {
        return mapper.update(wages);
    }

    @Override
    public void del(int id) {
        mapper.del(id);
    }
}
