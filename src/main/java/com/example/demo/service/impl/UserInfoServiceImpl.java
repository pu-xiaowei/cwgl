package com.example.demo.service.impl;

import com.example.demo.configs.Md5UtilSimple;
import com.example.demo.dao.UserInfoMapper;
import com.example.demo.entity.House;
import com.example.demo.entity.Role;
import com.example.demo.entity.UserInfo;
import com.example.demo.service.UserInfoService;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;
import com.example.demo.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserInfoServiceImpl implements UserInfoService {
    @Autowired
    private UserInfoMapper userInfoMapper;

    //注册用户
    @Override
    public int addUser(UserInfo userInfo) {
        //对密码进行MD5加密
        userInfo.setPassword(Md5UtilSimple.md5(userInfo.getPassword()));
        int result = userInfoMapper.addUser(userInfo);
        if (userInfo.getRoleid() == 2) {
            //如果是家庭管理员，则新增家庭
            House newHouse = new House();
            newHouse.setOwnerid(userInfo.getId());
            int r = userInfoMapper.addHouseId(newHouse);
            //添加家庭成功后，绑定到该用户
            if (r == 1) {
                userInfo.setHouseid(newHouse.getId().toString());
                result = userInfoMapper.update(userInfo);
            }
        }
        return result;
    }

    //添加用户
    @Override
    public int add(UserInfo userInfo) {
        if (userInfo.getPassword() != null) {
            //对密码进行MD5加密
            userInfo.setPassword(Md5UtilSimple.md5(userInfo.getPassword()));
        }
        int result = userInfoMapper.add(userInfo);
        if (userInfo.getRoleid() == null) {
            //如果是家庭管理员，则新增家庭
            House newHouse = new House();
            newHouse.setOwnerid(userInfo.getId());
            int r = userInfoMapper.addHouseId(newHouse);
            //添加家庭成功后，绑定到该用户
            if (r == 1) {
                userInfo.setHouseid(newHouse.getId().toString());
                result = userInfoMapper.update(userInfo);
            }
        }
        return result;
    }

    //修改用户密码
    @Override
    public int changePassword(UserInfo userInfo, String newPassword) {
        return userInfoMapper.changePassword(userInfo.getId(), newPassword);
    }

    //更新用户信息
    @Override
    public int update(UserInfo userInfo) {
        //对密码进行MD5加密
        userInfo.setPassword(Md5UtilSimple.md5(userInfo.getPassword()));
        return userInfoMapper.update(userInfo);
    }

    //删除用户
    @Override
    public int delete(String id) {
        return userInfoMapper.delete(id);
    }

    //判断用户是否存在
    @Override
    public boolean userIsExisted(UserInfo userInfo) {
        return userInfoMapper.userIsExisted(userInfo) > 0;
    }

    //获取用户所有信息
    @Override
    public UserInfo getUserInfo(UserInfo userInfo) {
        if (userInfo.getPassword() != null) {
            userInfo.setPassword(Md5UtilSimple.md5(userInfo.getPassword()));
        }

        return userInfoMapper.getUserInfo(userInfo);
    }

    @Override
    public Result getUsersByWhere(PageModel<UserInfo> model) {
        try {
            List<UserInfo> users = userInfoMapper.getUsersByWhere(model);
            if (users.size() >= 0) {
                Result<UserInfo> result = ResultUtil.success(users);
                result.setTotal(userInfoMapper.getTotalByWhere(model));
                if (result.getTotal() == 0) {
                    result.setMsg("没有查到相关数据");
                } else {
                    result.setMsg("数据获取成功");
                }
                return result;
            } else {
                return ResultUtil.unSuccess("没有找到符合条件的属性！");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @Override
    public List<Role> getAllRoles() {
        return userInfoMapper.getAllRoles();
    }

    @Override
    public List<Role> getRoles() {
        return userInfoMapper.getRoles();
    }

    @Override
    public Role getRoleById(String id) {
        return userInfoMapper.getRoleById(id);
    }

    @Override
    public int addRole(Role role) {
        return userInfoMapper.addRole(role);
    }

    @Override
    public int updateRole(Role role) {
        return userInfoMapper.updateRole(role);
    }

    @Override
    public int deleteRole(String id) {
        return userInfoMapper.deleteRole(id);
    }

    @Override
    public List<String> getAllPrivilege(String roleId) {
        return userInfoMapper.getAllPrivilege(roleId);
    }
}
