package com.example.demo.service.impl;

import com.example.demo.dao.CardMapper;
import com.example.demo.entity.Card;
import com.example.demo.service.CardService;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;
import com.example.demo.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardServiceImpl implements CardService {

    @Autowired
    CardMapper mapper;

    @Override
    public Result<Card> getCards(PageModel model) {
        try {
            List<Card> cardList = mapper.findByWhere(model);
            if (cardList.size() >= 0) {
                Result<Card> result = ResultUtil.success(cardList);
                result.setTotal(mapper.getTotalByWhere(model));
                if (result.getTotal() == 0) {
                    result.setMsg("没有查到相关数据");
                } else {
                    result.setMsg("数据获取成功");
                }
                return result;
            } else {
                return ResultUtil.unSuccess("获取数据失败！");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @Override
    public int addCard(Card card) {
        return mapper.add(card);
    }

    @Override
    public int updateCard(Card card) {
        return mapper.update(card);
    }

    @Override
    public void delCard(int id) {
        mapper.del(id);
    }
}
