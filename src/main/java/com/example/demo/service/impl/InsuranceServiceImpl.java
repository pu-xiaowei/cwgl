package com.example.demo.service.impl;

import com.example.demo.dao.InsuranceMapper;
import com.example.demo.entity.Insurance;
import com.example.demo.service.InsuranceService;
import com.example.demo.utils.PageModel;
import com.example.demo.utils.Result;
import com.example.demo.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InsuranceServiceImpl implements InsuranceService {

    @Autowired
    InsuranceMapper mapper;

    @Override
    public Result<Insurance> getInsurance(PageModel model) {
        try {
            List<Insurance> insuranceList = mapper.findByWhere(model);
            if (insuranceList.size() >= 0) {
                Result<Insurance> result = ResultUtil.success(insuranceList);
                result.setTotal(mapper.getTotalByWhere(model));
                if (result.getTotal() == 0) {
                    result.setMsg("没有查到相关数据");
                } else {
                    result.setMsg("数据获取成功");
                }
                return result;
            } else {
                return ResultUtil.unSuccess("获取数据失败！");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @Override
    public int addInsurance(Insurance insurance) {
        return mapper.add(insurance);
    }

    @Override
    public int updateInsurance(Insurance insurance) {
        return mapper.update(insurance);
    }

    @Override
    public void delInsurance(int id) {
        mapper.del(id);
    }
}
