package com.example.demo;

import com.example.demo.dao.UserInfoMapper;
import com.example.demo.entity.Role;
import com.example.demo.entity.UserInfo;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest()
class DemoApplicationTests {
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Test
    public void contextLoads() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(1);
//        userInfo.setUsername("pxw");
//        System.out.println(userInfo);
        UserInfo s = this.userInfoMapper.getUserInfo(userInfo);
        if (s==null){
            System.out.println("空的");
        }else {
            System.out.printf(s.toString());
        }

    }
    @Test
    public void tset_01(){
        List<Role> allRoles = userInfoMapper.getAllRoles();
        for (Role allRole : allRoles) {
            System.out.println(allRole);
        }

    }

}
