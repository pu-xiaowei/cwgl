﻿# Host: localhost  (Version: 5.7.26)
# Date: 2022-05-30 10:29:57
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "card"
#

CREATE TABLE `card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cardname` varchar(25) DEFAULT NULL COMMENT '卡片名称',
  `type` varchar(2) DEFAULT NULL COMMENT '卡片类型',
  `typename` varchar(255) DEFAULT NULL COMMENT '类型名称',
  `userid` int(11) NOT NULL COMMENT '用户id',
  `cardnum` varchar(50) DEFAULT NULL COMMENT '卡号',
  `createtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "card"
#

INSERT INTO `card` VALUES (2,'华润万家','R1','银行卡',1,'2','2022-04-15 13:50:29'),(3,'如家','R3','会员卡',1,'12321432','2022-04-15 14:08:40'),(4,'1','R4','其他',1,'1111','2022-04-15 14:54:03'),(5,'zgsskaa','R1','银行卡',1,'121212','2022-05-28 18:21:05'),(6,'中国建设银行','R1','银行卡',58,'901827213','2022-05-28 18:45:19'),(7,'中国建设银行','R1','银行卡',72,'123456789','2022-05-28 19:59:41'),(8,'中国银行','R1','银行卡',74,'1543224','2022-05-28 20:10:10'),(9,'爱上美丽','R5','其他',73,'44566212','2022-05-28 20:19:39'),(10,'会员卡','R3','会员卡',76,'1234566','2022-05-28 20:24:02'),(11,'中国邮政储蓄银行','R1','银行卡',77,'56431354','2022-05-28 20:28:07'),(12,'绿源超市vip','R2','超市卡',77,'656103251','2022-05-28 20:28:44'),(13,'中国招商银行','R1','银行卡',75,'5113498461','2022-05-28 20:32:44'),(14,'宜宾公交卡','R4','公交卡',78,'651613','2022-05-28 20:34:52'),(15,'中国建设银行','R1','银行卡',78,'64613131','2022-05-28 20:35:10'),(16,'宜宾公交','R4','公交卡',1,'123456789','2022-05-29 10:34:13');

#
# Structure for table "curaccount"
#

CREATE TABLE `curaccount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `houseid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `money` float(255,0) DEFAULT NULL,
  `paywayid` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `typeid_id` (`houseid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "curaccount"
#

INSERT INTO `curaccount` VALUES (37,1,1,'家庭资产',21881,NULL,'无'),(40,57,26,'家庭资产',6700,NULL,'无'),(41,60,26,'家庭资产',-1390,NULL,'无'),(42,59,26,'家庭资产',-850,NULL,'无'),(43,58,26,'家庭资产',9000,NULL,'无'),(44,72,35,'家庭资产',9371,NULL,'无'),(45,74,35,'家庭资产',6800,NULL,'无'),(46,73,35,'家庭资产',20890,NULL,'无'),(47,76,35,'家庭资产',2700,NULL,'无'),(48,77,35,'家庭资产',9285,NULL,'无'),(49,75,35,'家庭资产',9595,NULL,'无'),(50,78,35,'家庭资产',9801,NULL,'无');

#
# Structure for table "debt"
#

CREATE TABLE `debt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `houseid` varchar(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `residue` float(255,2) DEFAULT NULL,
  `curperiod` float(255,2) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "debt"
#

INSERT INTO `debt` VALUES (10,1,'1','房贷',7000.00,1000.00),(11,1,'1','测试贷款',440.00,92.00),(12,72,'35','房贷',100000.00,2000.00),(13,74,'35','车贷',200000.00,8000.00),(14,73,'35','房贷',100000.00,6500.00),(15,1,'1','房贷',200000.00,1800.00);

#
# Structure for table "house"
#

CREATE TABLE `house` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ownerid` int(11) NOT NULL COMMENT '户主编号',
  `address` varchar(255) DEFAULT NULL COMMENT '家庭住址',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `holderid` (`ownerid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "house"
#

INSERT INTO `house` VALUES (1,1,''),(26,57,NULL),(27,61,NULL),(31,66,NULL),(32,68,NULL),(33,70,NULL),(34,71,NULL),(35,72,NULL),(36,73,NULL),(37,74,NULL),(38,79,NULL);

#
# Structure for table "insurance"
#

CREATE TABLE `insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insuranceno` varchar(50) DEFAULT NULL COMMENT '保险单号',
  `insurancename` varchar(50) DEFAULT NULL COMMENT '保险名称',
  `start` date DEFAULT NULL COMMENT '生效日期',
  `end` date DEFAULT NULL COMMENT '截止日期',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "insurance"
#

INSERT INTO `insurance` VALUES (4,'123456789','中国人寿保险','2022-01-01','2029-12-31',72),(5,'134567778','意外伤害险','2022-05-28','2023-01-27',74),(6,'13333222','运动意外无忧险','2022-05-01','2023-05-22',73),(7,'1235569','车险','2022-05-18','2023-05-15',76),(8,'544613144','学平险','2022-05-18','2025-05-07',77),(9,'6546313','全家无忧险','2022-05-08','2022-06-10',75),(10,'6515636','宠物医疗保险','2022-05-03','2022-08-24',78);

#
# Structure for table "moneymanage"
#

CREATE TABLE `moneymanage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `money` varchar(255) DEFAULT NULL,
  `rate` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `houseid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "moneymanage"
#

INSERT INTO `moneymanage` VALUES (14,'信诚薪金宝','200.0','2.87',31,8),(15,'国寿光源180','200.0','2.64',31,8),(16,'平安享赢368','400.0','4.50',30,7),(17,'广发聚荣','300.0','5.31',30,7),(18,'汇添富','300.0','3.66',27,7),(19,'鹏华招华','200.0','5.09',27,7),(20,'licai','100.0','4',1,NULL),(22,'产品1','10000.0','0.05',53,24),(23,'鄢永洋','454.0','9',1,1),(24,'基金','40000.0','4',72,35),(25,'白酒基金','20000.0','6.25',74,35),(26,'华商xx股票','1300.0','3.52',73,35),(27,'xx医疗股票','2300.0','1.25',76,35),(28,'xx债券','120.0','0.23',77,35),(29,'基金','10000.0','3',1,1);

#
# Structure for table "payway"
#

CREATE TABLE `payway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payway` varchar(255) DEFAULT NULL,
  `extra` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "payway"
#

INSERT INTO `payway` VALUES (1,'支付宝',NULL),(2,'微信',NULL),(3,'银联',NULL),(4,'现金',NULL),(5,'其他',NULL);

#
# Structure for table "privilege"
#

CREATE TABLE `privilege` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `privilegeNumber` varchar(80) DEFAULT NULL COMMENT '权限编号',
  `privilegeName` varchar(80) DEFAULT NULL COMMENT '权限名称',
  `privilegeTipflag` char(4) DEFAULT NULL COMMENT '菜单级别',
  `privilegeTypeflag` char(4) DEFAULT NULL COMMENT '1启用 0禁用',
  `privilegeUrl` varchar(255) DEFAULT NULL COMMENT '权限URL',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "privilege"
#

INSERT INTO `privilege` VALUES (62,'001','收支管理','0','1','','&#xe698;'),(63,'001002','支出详情','1','1','pay_details','&#xe698;'),(64,'002','账户总览','0','1',NULL,'&#xe702;'),(65,'001001','收入详情','1','1','income_details','&#xe698;'),(66,'003','统计报表','0','1',NULL,'&#xe757;'),(67,'003001','统计报表','1','1','chart_line','&#xe757;'),(68,'004','家庭成员管理','0','1',NULL,'&#xe726;'),(69,'005','系统管理','0','1','','&#xe696;'),(70,'005001','用户管理','1','1','sys_adminusers','&#xe6b8;'),(71,'005002','角色管理','1','1','sys_roles','&#xe70b;'),(74,'004001','家庭成员信息','1','1','sys_users','&#xe726;'),(75,'002002','理财详情','1','1','asset_moneymanage_details','&#xe702;'),(76,'002001','活期资产','1','1','asset_account_details','&#xe702;'),(77,'002003','负债详情','1','1','asset_debt_details','&#xe702;'),(78,'002004','理财推荐','1','1','asset_product_details','&#xe702;'),(79,'002005','卡类管理','1','1','asset_card_details','&#xe702;'),(80,'002006','保险管理','1','1','asset_insurance_details','&#xe702;'),(81,'002007','工资录入','1','1','asset_wages_details','&#xe702;');

#
# Structure for table "product"
#

CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL COMMENT '风险等级',
  `remark` varchar(255) DEFAULT NULL COMMENT '风险描述',
  `loss` varchar(255) DEFAULT NULL COMMENT '亏损概率',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "product"
#

INSERT INTO `product` VALUES (1,'国债','R1','谨慎型、低风险','零'),(2,'存款类产品','R1','谨慎型、低风险','零'),(3,'保本保收益类产品','R1','谨慎型、低风险','零'),(4,'保本浮动收益类产品','R1','谨慎型、低风险','零'),(5,'银行间市场债券','R2','稳健型、中低风险','接近零'),(6,'交易所市场债券','R2','稳健型、中低风险','接近零'),(7,'资金拆借','R2','稳健型、中低风险','接近零'),(8,'信托计划','R2','稳健型、中低风险','接近零'),(9,'其他金融资产','R2','稳健型、中低风险','接近零'),(10,'债券','R3','平衡型、中等风险','较低'),(11,'同业存放','R3','平衡型、中等风险','较低'),(12,'股票','R3','平衡型、中等风险','较低'),(13,'商品','R3','平衡型、中等风险','较低'),(14,'外汇','R3','平衡型、中等风险','较低'),(15,'净值型理财','R3','平衡型、中等风险','较低'),(16,'混合型基金','R3','平衡型、中等风险','较低'),(17,'私募基金','R4','进取型、中高风险','较高'),(18,'信托产品','R4','进取型、中高风险','较高'),(19,'股票基金','R4','进取型、中高风险','较高'),(20,'指数基金','R4','进取型、中高风险','较高');

#
# Structure for table "role"
#

CREATE TABLE `role` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rolename` varchar(255) DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`roleid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "role"
#

INSERT INTO `role` VALUES (1,'系统管理员'),(2,'家庭管理员'),(3,'普通用户'),(4,'外部人员');

#
# Structure for table "roleprivilieges"
#

CREATE TABLE `roleprivilieges` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `roleID` int(11) DEFAULT NULL COMMENT '角色维护表主键',
  `privilegeID` int(11) DEFAULT NULL COMMENT '权限维护表主键',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `roleID` (`roleID`) USING BTREE,
  KEY `privilegeID` (`privilegeID`) USING BTREE,
  CONSTRAINT `roleprivilieges_ibfk_1` FOREIGN KEY (`roleID`) REFERENCES `role` (`roleid`),
  CONSTRAINT `roleprivilieges_ibfk_2` FOREIGN KEY (`privilegeID`) REFERENCES `privilege` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1114 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "roleprivilieges"
#

INSERT INTO `roleprivilieges` VALUES (1023,2,65),(1024,2,63),(1025,2,75),(1026,2,76),(1027,2,77),(1028,2,79),(1029,2,80),(1030,2,81),(1031,2,67),(1032,2,74),(1033,2,62),(1034,2,64),(1035,2,66),(1036,2,68),(1070,3,65),(1071,3,63),(1072,3,75),(1073,3,76),(1074,3,77),(1075,3,79),(1076,3,80),(1077,3,81),(1078,3,67),(1079,3,62),(1080,3,64),(1081,3,66),(1099,1,65),(1100,1,63),(1101,1,75),(1102,1,76),(1103,1,77),(1104,1,79),(1105,1,80),(1106,1,81),(1107,1,67),(1108,1,70),(1109,1,71),(1110,1,62),(1111,1,64),(1112,1,66),(1113,1,69);

#
# Structure for table "type"
#

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "type"
#

INSERT INTO `type` VALUES (1,'支出'),(2,'收入');

#
# Structure for table "bill"
#

CREATE TABLE `bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `money` float(10,2) DEFAULT NULL COMMENT '金额',
  `typeid` int(11) NOT NULL COMMENT '类型 1 收入 2 支出',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `paywayid` int(11) DEFAULT NULL COMMENT '支付方式',
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '交易时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `userid` (`userid`) USING BTREE,
  KEY `type` (`typeid`) USING BTREE,
  KEY `payway` (`paywayid`) USING BTREE,
  CONSTRAINT `bill_ibfk_2` FOREIGN KEY (`typeid`) REFERENCES `type` (`id`),
  CONSTRAINT `bill_ibfk_3` FOREIGN KEY (`paywayid`) REFERENCES `payway` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=348 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "bill"
#

INSERT INTO `bill` VALUES (261,'工资',57,7500.00,2,'2022年4月份工资',3,'2022-05-09 16:30:37'),(262,'家教',57,1000.00,2,'',2,'2022-05-09 16:30:57'),(263,'基金',57,2600.00,2,'',3,'2022-05-09 16:31:31'),(264,'购买衣服',57,1500.00,1,'',2,'2022-05-09 16:31:55'),(265,'日常开销',57,2600.00,1,'',4,'2022-05-09 16:32:11'),(266,'医疗保险',57,300.00,1,'',2,'2022-05-09 16:32:30'),(267,'缴纳书费',60,240.00,1,'',4,'2022-05-09 16:35:33'),(268,'吃饭',60,1000.00,1,'',2,'2022-05-09 16:35:46'),(269,'交通费',60,150.00,1,'',4,'2022-05-09 16:36:07'),(270,'兼职',59,300.00,2,'',2,'2022-05-09 16:40:01'),(271,'家教',59,200.00,2,'',4,'2022-05-09 16:40:54'),(272,'日常花销',59,1000.00,1,'',2,'2022-05-09 16:41:26'),(273,'聚餐娱乐',59,350.00,1,'',2,'2022-05-09 16:41:41'),(274,'工资',58,12000.00,2,'2022年4月份工资',3,'2022-05-09 16:44:08'),(275,'奖金',58,1500.00,2,'',4,'2022-05-09 16:44:28'),(276,'分红',58,5500.00,2,'',3,'2022-05-09 16:44:40'),(278,'日常开销',58,2500.00,1,'',2,'2022-04-09 16:45:21'),(279,'交通花销',58,1000.00,1,'',2,'2022-05-09 16:45:43'),(280,'消费',61,199.00,1,'',1,'2022-05-16 11:55:24'),(281,'1111',1,1000.00,1,'111',5,'2022-05-16 18:59:00'),(282,'房贷',1,1000.00,1,'还款',5,'2022-05-16 19:00:11'),(283,'房贷',1,1000.00,1,'还款',5,'2022-05-16 19:02:59'),(286,'测试贷款',1,92.00,1,'还款',5,'2022-05-17 09:32:41'),(287,'测试贷款',1,92.00,1,'还款',5,'2022-05-17 09:33:00'),(294,'测试贷款',1,92.00,1,'还款',5,'2022-05-19 08:46:03'),(295,'测试贷款',1,92.00,1,'还款',5,'2022-05-19 08:46:12'),(296,'房贷',1,1000.00,1,'还款',5,'2022-05-19 08:46:18'),(297,'测试贷款',1,92.00,1,'还款',5,'2022-05-19 08:46:21'),(298,'测试贷款',1,92.00,1,'还款',5,'2022-05-19 08:46:24'),(305,'食品',1,900.00,1,'',1,'2022-05-28 18:39:39'),(306,'人情',58,10000.00,1,'',1,'2022-05-28 19:10:44'),(308,'工资',72,10370.90,2,'工资录入',5,'2022-05-28 19:47:15'),(309,'工资',74,12000.00,2,'工资录入',5,'2022-05-28 20:11:56'),(310,'食品',74,500.00,1,'一般伙食',1,'2022-05-28 20:12:59'),(311,'出行',74,200.00,1,'公共交通费用',1,'2022-05-28 20:13:27'),(312,'购物',74,500.00,1,'1111,618活动不理智消费',3,'2022-05-28 20:14:01'),(314,'住宿',74,2000.00,1,'房贷',3,'2022-05-28 20:15:02'),(315,'工资',73,20890.00,2,'工资录入',5,'2022-05-28 20:20:17'),(339,'人情',76,500.00,1,'应酬',1,'2022-05-28 20:22:08'),(340,'工资',76,14200.00,2,'工资录入',5,'2022-05-28 20:25:31'),(341,'工资',77,9285.00,2,'工资录入',5,'2022-05-28 20:29:31'),(342,'工资',75,9595.00,2,'工资录入',5,'2022-05-28 20:31:09'),(343,'工资',78,9801.00,2,'工资录入',5,'2022-05-28 20:33:27'),(344,'医疗',72,1000.00,1,'医药费',4,'2022-05-28 20:41:01'),(345,'白嫖',1,100.00,2,'',4,'2022-05-29 10:32:30'),(346,'食品',1,100.00,1,'',2,'2022-05-29 10:32:45'),(347,'工资',1,11850.00,2,'工资录入',5,'2022-05-29 10:34:46');

#
# Structure for table "user"
#

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(255) DEFAULT NULL COMMENT '账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `realname` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `roleid` int(11) NOT NULL DEFAULT '3' COMMENT '角色编号',
  `houseid` int(11) DEFAULT NULL COMMENT '所属家庭编号',
  `photo` varchar(255) DEFAULT NULL COMMENT '用户头像',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `houseid` (`houseid`) USING BTREE,
  KEY `roleid` (`roleid`) USING BTREE,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`houseid`) REFERENCES `house` (`id`),
  CONSTRAINT `user_ibfk_3` FOREIGN KEY (`roleid`) REFERENCES `role` (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "user"
#

INSERT INTO `user` VALUES (1,'admin','21232f297a57a5a743894ae4a801fc3','管理员',1,1,NULL),(57,'mother','21232f297a57a5a743894ae4a801fc3','母亲',2,26,NULL),(58,'Tom','21232f297a57a5a743894ae4a801fc3','父亲',2,26,NULL),(60,'daughter','21232f297a57a5a743894ae4a801fc3','女儿',3,26,NULL),(72,'pxw','c33367701511b4f62ec61ded352059','蒲小伟',2,35,NULL),(73,'yyy','e1adc3949ba59abbe56e057f2f883e','鄢永洋',2,35,NULL),(74,'zx','e6c760b3216a51c656c5861d72d5bf62','郑鑫',2,35,NULL),(75,'tys','e1adc3949ba59abbe56e057f2f883e','谭宇声',3,35,NULL),(76,'cxr','d41d8cd98f0b24e980998ecf8427e','蔡欣锐',2,35,NULL),(77,'lys','e1adc3949ba59abbe56e057f2f883e','罗义松',3,35,NULL),(78,'lsh','e1adc3949ba59abbe56e057f2f883e','林颂好',3,35,NULL),(79,'a','e1adc3949ba59abbe56e057f2f883e','张三',2,38,NULL);

#
# Structure for table "wages"
#

CREATE TABLE `wages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `basewage` float(255,0) DEFAULT NULL COMMENT '基本工资',
  `gradewage` float(255,0) DEFAULT NULL COMMENT '薪级工资',
  `performance` float(255,0) DEFAULT NULL COMMENT '绩效奖金',
  `otherwage` float(255,0) DEFAULT NULL COMMENT '其余补贴',
  `fiveinsurance` float(255,0) DEFAULT NULL COMMENT '五险一金扣款',
  `tax` float(255,0) DEFAULT NULL COMMENT '个人所得税',
  `otherpayment` float(255,0) DEFAULT NULL COMMENT '其它扣款',
  `sumwage` varchar(255) DEFAULT NULL COMMENT '总工资',
  `userid` int(11) NOT NULL COMMENT '用户id',
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "wages"
#

INSERT INTO `wages` VALUES (1,1,2,45,67,5,2,3,'105',1,'2022-04-16 22:39:31'),(3,1333,1122,333,2223,444,555,33,'3979',1,'2022-04-17 15:43:36'),(4,92,0,0,0,0,0,0,'92',1,'2022-04-17 10:14:01'),(5,1000,100,100,500,530,200,100,'870',1,'2022-04-18 07:41:57'),(6,100,100,10,10,10,10,10,'190',1,'2022-04-19 08:48:01'),(7,1,1,11,10,1,1,1,'20',1,'2022-04-19 08:49:03'),(8,2000,2000,100,89,31,139,0,'4019',1,'2022-05-28 19:16:22'),(9,8000,3000,500,300,1000,429,0,'10370.9',72,'2022-05-28 19:47:15'),(10,27000,1000,1000,500,300,300,0,'28900',74,'2022-05-28 20:11:56'),(11,20000,1000,500,100,300,350,60,'20890',73,'2022-05-28 20:20:17'),(12,15000,200,200,200,200,600,600,'14200',76,'2022-05-28 20:25:31'),(13,9500,300,150,320,450,412,123,'9285',77,'2022-05-28 20:29:31'),(14,9540,300,150,20,100,315,0,'9595',75,'2022-05-28 20:31:08'),(15,10000,120,510,46,462,412,1,'9801',78,'2022-05-28 20:33:27'),(16,10000,2000,300,100,400,150,0,'11850',1,'2022-05-29 10:34:46');
